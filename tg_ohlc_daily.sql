-- check fn_check_symbol_trigger.sql for details

-- Creates triggers that will check the validity of symbols entered into each table

BEGIN;

CREATE TRIGGER validate_symbol_ohlc_daily_nasdaq_trigger BEFORE INSERT OR UPDATE ON ohlc_daily_nasdaq
FOR EACH ROW EXECUTE PROCEDURE check_symbol('NASDAQ');

CREATE TRIGGER validate_symbol_ohlc_daily_hkex_trigger BEFORE INSERT OR UPDATE ON ohlc_daily_hkex
FOR EACH ROW EXECUTE PROCEDURE check_symbol('HKEX');

CREATE TRIGGER validate_symbol_ohlc_daily_sse_trigger BEFORE INSERT OR UPDATE ON ohlc_daily_sse
FOR EACH ROW EXECUTE PROCEDURE check_symbol('SSE');

CREATE TRIGGER validate_symbol_ohlc_daily_szse_trigger BEFORE INSERT OR UPDATE ON ohlc_daily_szse
FOR EACH ROW EXECUTE PROCEDURE check_symbol('SZSE');

CREATE TRIGGER validate_symbol_ohlc_daily_asx_trigger BEFORE INSERT OR UPDATE ON ohlc_daily_asx
FOR EACH ROW EXECUTE PROCEDURE check_symbol('ASX');

CREATE TRIGGER validate_symbol_ohlc_daily_bse_trigger BEFORE INSERT OR UPDATE ON ohlc_daily_bse
FOR EACH ROW EXECUTE PROCEDURE check_symbol('BSE');

CREATE TRIGGER validate_symbol_ohlc_daily_nse_trigger BEFORE INSERT OR UPDATE ON ohlc_daily_nse
FOR EACH ROW EXECUTE PROCEDURE check_symbol('NSE');
