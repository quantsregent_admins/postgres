-- hOW TO CREATE A CONSTRAINT THAT CHECKES CONDITIONS FROM ANOTHER TABLE?

-- This should be achieved using a function and a trigger

-- Creates function check_symbol
-- Creates trigger validate_symbol_ohlc_daily_nyse_trigger

-- validate_symbol_ohlc_daily_nyse_trigger is triggered whenever an entry is added or
-- updated in the ohlc_daily_nyse table. Trigger will call the function check_symbol()
-- with the input 'NYSE' as the exchange. check_symbol() will check whether the
-- symbol belongs to the exchange 'NYSE' in the table symbol_master. If it does not
-- it will raise an exception

BEGIN;

CREATE OR REPLACE FUNCTION check_symbol() RETURNS TRIGGER
AS $$
BEGIN
    IF (NEW.symbol NOT IN (select symbol from symbol_master where exchange_code=TG_ARGV[0])) THEN
	RAISE EXCEPTION 'Symbol % does not exist in Exchange % !!', NEW.symbol, TG_ARGV[0];
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER validate_symbol_ohlc_daily_nyse_trigger BEFORE INSERT OR UPDATE ON ohlc_daily_nyse
FOR EACH ROW EXECUTE PROCEDURE check_symbol('NYSE');
