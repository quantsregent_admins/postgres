BEGIN;

CREATE TABLE ohlc_daily_asx (
    symbol VARCHAR(10) NOT NULL,
    trade_date DATE NOT NULL,
    high REAL,
    low REAL,
    open REAL,
    close REAL,
    volume DOUBLE PRECISION,
    adjclose REAL,
    PRIMARY KEY(symbol, trade_date)
);
