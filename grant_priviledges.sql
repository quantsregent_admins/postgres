BEGIN;

GRANT select,insert,update,delete,truncate
on all TABLES in SCHEMA data TO gayan_rw;

GRANT select
on all TABLES in SCHEMA data TO gayan_ro;
