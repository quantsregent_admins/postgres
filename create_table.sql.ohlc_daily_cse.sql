BEGIN;

CREATE TABLE ohlc_daily_cse (
    symbol VARCHAR(10) NOT NULL,
    trade_date DATE NOT NULL,
    high REAL,
    low REAL,
    open REAL,
    close REAL,
    volume DOUBLE PRECISION,
    adjclose REAL,
    PRIMARY KEY(symbol, trade_date)
);

CREATE TRIGGER validate_symbol_ohlc_daily_cse_trigger BEFORE INSERT OR UPDATE ON ohlc_daily_cse
FOR EACH ROW EXECUTE PROCEDURE check_symbol('CSE');
